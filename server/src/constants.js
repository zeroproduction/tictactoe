module.exports = {
    ROOM_ID: "tictactoe",
    USER_CONSTANTS: {
        COMPUTER: "computer",
        USER: "user"
    },
    CACHE_CONSTANTS : {
        SAVE_SCORE: "save-score"
    },
    PUBSUB_CONSTANTS: {
        UPDATE_GAME_LAYOUT: "update-game-layout",
        UPDATE_GAME_MESSAGE: "update-game-message"
    },
    SOCKET_CONSTANTS: {
        UPDATE_GAME_STATE: "update-game-state",
        GET_GAME_LAYOUT: "get-game-layout"
    }
}