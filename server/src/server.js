import http from "http";
import express from "express";
import SocketManager from "@Socket/SocketManager";

var app     = express();
var port    = process.env.PORT || 3000;
var server  = http.createServer(app);
var io      = require("socket.io")(server);

server.listen(port, () => console.log('Server listening at port %d', port));

let socketManager = new SocketManager();

io.sockets.on('connection', async (client) => socketManager.SetSocketEvents(io, client));