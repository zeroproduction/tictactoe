import PubSub from "pubsub-js";

import GameState from "@Engine/GameState";
import RenderLayout from "@Engine/RenderLayout";
import GameMessages from "@Engine/GameMessages";
import GameCache from "@Engine/GameCache";
import Constants from "@Root/constants";
import Combinations from "@Model/Combinations";
import User from "@Model/User";

export default class GameEngineService
{
    constructor()
    {
        this.GameCache = new GameCache();
    }

    _createPlayers = () =>
    {
        let usersList = [];
        let user = new User();
        user.UserType = Constants.USER_CONSTANTS.USER;

        let computer = new User();
        computer.UserType = Constants.USER_CONSTANTS.COMPUTER;

        usersList.push(user);
        usersList.push(computer);
        return usersList;
    }

    StartGame = () => this._initGame(this._createPlayers());
    RestartGame = () => this._initGame(this.GameCache.GetCache(Constants.CACHE_CONSTANTS.SAVE_SCORE));

    /**
     * Initializes game
     * @param users - users array
     * @private
     */
    _initGame = (users) =>
    {
        this.GameState = new GameState(users);
        this.RenderLayout = new RenderLayout();
        this.RenderLayout.Buildlayout();
        this.UpdateUIState();
        this.AIMovementAction();
    }

    /**
     * Checks game state have game ended
     * @returns {boolean}
     * @private
     */
    _hasGameEnded = () =>
    {
        let gameEnded = false;

        if (this.GameState.CheckIsGameOver())
        {
            gameEnded = true;
        }
        else if (this.GameState.GameEndedAndDraw())
        {
            this.GameState.SetGameNotInProgress();
            this.GameState.SetWinnerValue(false);
            gameEnded = true;
        }
        this.UpdateUIState();
        return gameEnded;
    }

    /**
     * Movement action for user
     * @param index - square index position
     */
    MovementAction = (index) =>
    {
        if (this._hasGameEnded()) return;
        this.GameState.IncrementMovesMade();

        // make a move
        this.RenderLayout.SetSquareValueById(index, this.GameState.CurrentTurn);

        this._checkCombination();
        if (this._hasGameEnded()) return;
        this.GameState.SwitchPlayersTurn();
        this.UpdateUIState();
        this.AIMovementAction();
    }

    /**
     * Checks winning combination
     * @private
     */
    _checkCombination = () =>
    {
        Combinations.WinningCombinations.forEach((wc) =>
        {
            if (this.GameState.InProgress && !this.GameState.Winner && this.RenderLayout.MarkSquareCombinationSelected(wc))
            {
                this.GameState.PlayersContainer.IncrementActiveUserScore();
                this.GameState.PlayersContainer.IncrementGamesCount();
                this.GameState.SetGameNotInProgress();
                this.GameState.SetWinnerValue(true);
                this.GameCache.SetCache(
                    Constants.CACHE_CONSTANTS.SAVE_SCORE, this.GameState.PlayersContainer.PlayersList);
            }

            this.UpdateUIState();
        });
    }

    /**
     * Simulates movement for computer
     */
    AIMovementAction = () =>
    {
        if (!this.GameState.PlayersContainer.IsActiveUserComputer() || this._hasGameEnded()) return;
        setTimeout(() =>
        {
            // gets unselected square indexes
            let availableSquares = this.RenderLayout.GetUnselectedSquares();
            // Get new random index id
            let index = Math.floor(Math.random() * (this.RenderLayout.GetUnselectedSquares().length - 1));
            this.MovementAction(availableSquares[index]);
        }, 800);
    }

    /**
     * Sends PubSub event to socket manager for updating frontend UI state
     */
    UpdateUIState = () =>
    {
        PubSub.publish(Constants.PUBSUB_CONSTANTS.UPDATE_GAME_LAYOUT, {
            squares: this.RenderLayout.GetAllSquares(),
            message: new GameMessages(this.GameState).GetInfoMessage(),
            players: this.GameState.PlayersContainer.PlayersList,
            activePlayer: this.GameState.PlayersContainer.IsActiveUserComputer(),
            winner: this.GameState.Winner,
            activePlayerLabel: this.GameState.CurrentTurn
        });
    }

    /**
     * @returns Squares Array
     */
    GetAllSquares = () => this.RenderLayout.GetAllSquares();
}