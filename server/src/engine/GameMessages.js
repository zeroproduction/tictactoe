export default class GameMessages
{
    constructor(gameState)
    {
        this.GameState = gameState;
    }

    /**
     * Returns current turn, winner or draw message
     * @returns {string}
     * @constructor
     */

    GetInfoMessage()
    {
        if (!this.GameState.InProgress && !this.GameState.Winner) return "It was a draw!!";

        let message = "It is " + this.GameState.CurrentTurn + " turn";
        let winner = "Winner is " + this.GameState.CurrentTurn;
        return this.GameState.InProgress ? message : winner;
    }
}