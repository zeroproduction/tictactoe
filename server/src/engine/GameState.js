import Game from "@Model/Game";
import PlayersContainer from "@Engine/PlayersContainer";

export default class GameState
{
    SquaresCount = 9;
    MovesMade = 0;
    Winner = false;
    InProgress = true;
    CurrentTurn = Game.O;
    PlayersContainer = null;

    constructor(users)
    {
        this.PlayersContainer = new PlayersContainer();
        this.SetUsersList(users);
    }

    CheckIsGameOver = () => this.Winner && !this.InProgress;
    GameEndedAndDraw = () => this.MovesMade === this.SquaresCount && !this.InProgress;
    IncrementMovesMade = () => this.MovesMade++;

    SetGameNotInProgress = () => this.InProgress = false;
    SetWinnerValue = () => this.Winner = true;
    SetUsersList = (users) => this.PlayersContainer.SetUsersListAndAssignGameLabel(users);

    /**
     * Switches players array and assign CurrentTurn new value
     * @constructor
     */
    SwitchPlayersTurn = () =>
    {
        this.PlayersContainer.SwitchPlayersTurn();
        this.CurrentTurn = this.PlayersContainer.GetActiveUser().GameLabel;
    };
}