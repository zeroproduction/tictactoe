const shuffle = require('shuffle-array');
import Constants from "@Root/constants";
import Game from "@Model/Game";

export default class PlayersContainer
{
    PlayersList = [];

    SwitchPlayersTurn = () => this.PlayersList.reverse();
    GetActiveUser = () => this.PlayersList[0];
    IsActiveUserComputer = () => this.GetActiveUser().UserType == Constants.USER_CONSTANTS.COMPUTER;
    IncrementActiveUserScore = () => this.PlayersList[0].Score++;
    IncrementGamesCount = () => this.PlayersList.map(item => item.Games++);

    /**
     * Set users list, shuffles and assign GameLabels
     * @param Array - usersList
     */
    SetUsersListAndAssignGameLabel = (usersList) =>
    {
        shuffle(usersList);
        usersList[0].GameLabel = Game.O;
        usersList[1].GameLabel = Game.X;
        this.PlayersList = usersList;
    };
}