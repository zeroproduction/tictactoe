import NodeCache from "node-cache";
export default class GameCache
{
    Cache = null;

    constructor()
    {
        this.Cache = new NodeCache(
        {
            stdTTL: 100,
            checkperiod: 120,
            deleteOnExpire: true
        });
    }

    SetCache = (key, value) => this.Cache.set(key, value, 10000);
    GetCache = (key) => this.Cache.get(key);
}