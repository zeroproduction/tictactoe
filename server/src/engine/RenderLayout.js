import Square from "@Model/Square";

export default class RenderLayout
{
    Squares = null;

    constructor(count = 9)
    {
        this.SquaresCount = count;
    }

    GetAllSquares = () => this.Squares;

    /**
     * Build game squares and returns array
     * @returns {any[]}
     */
    Buildlayout = () => this.Squares = new Array(this.SquaresCount)
        .fill().map((s, i) => new Square(i));

    /**
     * Filters out unselected squares
     * @returns unselected Squares Array
     */
    GetUnselectedSquares = () => this.Squares.map(item =>
    {
        if (!item.Selected) return item.Index;
    }).filter(function( element ) {
        return element !== undefined;
    });

    /**
     * Marks Square selected by Square index
     * @param i - Square position index
     * @param labelvalue - Square label value (X or O)
     */
    SetSquareValueById = (i, labelvalue) =>
    {
        if (this.Squares[i].Selected) return false;
        this.Squares[i].LabelValue = labelvalue;
        this.Squares[i].Selected = true;
    };

    /**
     * Checks winning combination
     * @param combination
     * @returns {boolean}
     */
    MarkSquareCombinationSelected = (combination) =>
    {
        const [first, second, third] = combination;
        const squareFirst = this.Squares[first];
        const squareSecond = this.Squares[second];
        const squareThird = this.Squares[third];

        let hasCombination = false;
        if (squareFirst.LabelValue &&
            squareFirst.LabelValue === squareSecond.LabelValue &&
            squareFirst.LabelValue === squareThird.LabelValue)
        {
            squareFirst.IsHighlighted = squareSecond.IsHighlighted = squareThird.IsHighlighted = true;

            this.Squares[first] = squareFirst;
            this.Squares[second] = squareSecond;
            this.Squares[third] = squareThird;
            hasCombination = true;
        }
        return hasCombination;
    }
}