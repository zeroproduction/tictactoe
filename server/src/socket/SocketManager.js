import PubSub from "pubsub-js";
import Constants from "@Root/constants";
import GameEngineService from "@Service/GameEngineService";

export default class SocketManager
{
    constructor()
    {
        this.GameEngineService = new GameEngineService();
    }

    /**
     * @param io - SocketIO
     * @param client - Socket client
     */
    SetSocketEvents = (io, client) =>
    {
        client.join(Constants.ROOM_ID);
        client.on("start-game", (data) => this.StartGame(io));
        client.on("restart-game", (data) => this.GameEngineService.RestartGame());
        client.on("make-action", data => this.GameEngineService.MovementAction(data.index));
        client.on('disconnect', () => this.Disconnect(client));
        client.on('error', (error) => console.log("[SocketError]", error));

        PubSub.subscribe(
            Constants.PUBSUB_CONSTANTS.UPDATE_GAME_LAYOUT,
            (msg, data) => io.to(Constants.ROOM_ID).emit(Constants.SOCKET_CONSTANTS.UPDATE_GAME_STATE, {data: data}));
    }

    /**
     * Starts game
     * @param io - SocketIO
     */
    StartGame = (io) =>
    {
        this.GameEngineService.StartGame();
        let squares = this.GameEngineService.GetAllSquares();
        io.to(Constants.ROOM_ID).emit(
            Constants.SOCKET_CONSTANTS.GET_GAME_LAYOUT, {squares: squares});
    }

    /**
     * SocketIO disconnect event - clears all listeners
     * @param client
     */
    Disconnect = (client) =>
    {
        client.removeAllListeners();
        PubSub.unsubscribe(Constants.PUBSUB_CONSTANTS.UPDATE_GAME_LAYOUT);
    }
}