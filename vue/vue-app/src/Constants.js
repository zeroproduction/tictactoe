module.exports = {
    SOCKET_URL: "localhost:3000",
    SOCKET_EVENTS: {
        MAKE_ACTION: "make-action",
        RESTART_GAME: "restart-game",
        UPDATE_GAME_STATE: "update-game-state",
        GET_GAME_LAYOUT: "get-game-layout",
        START_GAME: "start-game"
    }
}