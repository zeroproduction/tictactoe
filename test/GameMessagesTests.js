import Game from "@Model/Game";

const expect = require("chai").expect;
import TestUserHelper from "./helpers/TestUserHelpers";
import Constants from "@Root/constants";
import GameState from "@Engine/GameState";
import GameMessages from "@Engine/GameMessages";

describe("GameMessagesTests", function() {
    let gameState = null;

    describe("Get a draw Message", function() {
        gameState = new GameState(
            TestUserHelper, Constants.USER_CONSTANTS.COMPUTER);
        gameState.InProgress = false;
        gameState.Winner = false;
        const messages = new GameMessages(gameState);

        it("Returned draw message", function() {
            expect(messages.GetInfoMessage()).any.to.contains("draw");
        });
    });
    describe("Get user X message", function() {
        gameState = new GameState(
            TestUserHelper, Constants.USER_CONSTANTS.COMPUTER);
        gameState.Winner = true;
        gameState.InProgress = false;
        gameState.CurrentTurn = Game.X;
        const messages = new GameMessages(gameState);

        it("User won message", function() {
            expect(messages.GetInfoMessage()).any.to.contains("Winner is " + Game.X);
        });
    });
    describe("Get computer O message", function() {
        gameState = new GameState(
            TestUserHelper, Constants.USER_CONSTANTS.COMPUTER);
        gameState.Winner = true;
        gameState.InProgress = false;
        gameState.CurrentTurn = Game.O;
        const messages = new GameMessages(gameState);

        console.log("[Get computer O message]", messages.GetInfoMessage());

        it("Computer won message", function() {
            expect(messages.GetInfoMessage()).any.to.contains("Winner is " + Game.O);
        });
    });
});