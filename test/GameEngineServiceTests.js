import Game from "@Model/Game";

const expect = require("chai").expect;
import GameEngineService from "@Service/GameEngineService";
import GameMessages from "@Engine/GameMessages";

describe("GameEngineServiceTests", function() {

    describe("MovementAction", function() {
        const gameEngineService = new GameEngineService();
        let squareSelectedIndex = 1;
        gameEngineService.StartGame();
        gameEngineService.MovementAction(squareSelectedIndex);

        it("MovesMade incremented", function() {
            expect( gameEngineService.GameState.MovesMade).to.equal(1);
        });

        it("Is Winner false", function() {
            expect( gameEngineService.GameState.Winner).to.equal(false);
        });

        it("Is InProgress false", function() {
            expect( gameEngineService.GameState.InProgress).to.equal(true);
        });
    });

    describe("Simulate draw state", function()
    {
        const gameEngineService = new GameEngineService();

        gameEngineService.StartGame();

        gameEngineService.RenderLayout.SetSquareValueById(0, Game.O);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(1, Game.X);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(2, Game.O);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.RenderLayout.SetSquareValueById(3, Game.X);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(4, Game.O);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(5, Game.X);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.RenderLayout.SetSquareValueById(6, Game.X);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(7, Game.O);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(8, Game.X);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.GameState.InProgress = false;
        gameEngineService.GameState.Winner = false;

        it("Moves Made equals 9", function() {
            expect(gameEngineService.GameState.MovesMade)
                .to.equals(gameEngineService.GameState.SquaresCount);
        });

        it("Draw state", function() {
            let message = new GameMessages(gameEngineService.GameState);
            expect(message.GetInfoMessage()).to.contains("draw");
        });
    });

    describe("Simulate win state", function()
    {
        const gameEngineService = new GameEngineService();

        gameEngineService.StartGame();

        gameEngineService.RenderLayout.SetSquareValueById(0, Game.O);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(1, Game.X);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(2, Game.O);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.RenderLayout.SetSquareValueById(3, Game.X);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(4, Game.O);
        gameEngineService.GameState.IncrementMovesMade();
        gameEngineService.RenderLayout.SetSquareValueById(5, Game.X);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.RenderLayout.SetSquareValueById(6, Game.O);
        gameEngineService.GameState.IncrementMovesMade();

        gameEngineService.GameState.InProgress = false;
        gameEngineService.GameState.Winner = true;

        it("Moves Made equals 7", function() {
            expect(gameEngineService.GameState.MovesMade)
                .to.equals(gameEngineService.GameState.SquaresCount - 2);
        });

        it("Winner is O state", function() {
            let message = new GameMessages(gameEngineService.GameState);
            expect(message.GetInfoMessage()).to.contains("Winner is " + Game.O);
        });
    });
});