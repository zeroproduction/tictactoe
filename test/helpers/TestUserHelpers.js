import Game from "@Model/Game";
import User from "@Model/User";
import UserType from "@Model/UserType";

const player1 = new User();
player1.UserType = UserType.Regular;
player1.GameLabel = Game.O;

const player2 = new User();
player2.UserType = UserType.Computer;
player2.GameLabel = Game.X;

const TestPlayersUserVsComputer = [player1, player2];
export default TestPlayersUserVsComputer;