import Game from "@Model/Game";

const expect = require("chai").expect;
import Constants from "@Root/constants";
import PlayersContainer from "@Engine/PlayersContainer";
import TestPlayersUserVsComputer from "./helpers/TestUserHelpers";

describe("PlayerContainerTests", function() {

    let playersContainer = new PlayersContainer();

    describe("Set players", function() {
        playersContainer.SetUsersListAndAssignGameLabel(TestPlayersUserVsComputer);
        it("Has 2 players", function() {
            expect(playersContainer.PlayersList.length).to.deep.equal(2);
        });
        it("Has 1 user", function() {
            playersContainer.PlayersList.filter(item =>
            {
                if (item.UserType !== Constants.USER_CONSTANTS.USER) return;
                expect(item.UserType).to.deep.equal(Constants.USER_CONSTANTS.USER);
            });
        });
        it("Has 1 computer", function() {
            playersContainer.PlayersList.filter(item =>
            {
                if (item.UserType !== Constants.USER_CONSTANTS.COMPUTER) return;
                expect(item.UserType).to.deep.equal(Constants.USER_CONSTANTS.COMPUTER);
            });
        });
        it("Has X label player", function() {
            playersContainer.PlayersList.filter(item =>
            {
                if (item.GameLabel !== Game.X) return;
                expect(item.GameLabel).to.deep.equal(Game.X);
            });
        });
        it("Has O label player", function() {
            playersContainer.PlayersList.filter(item =>
            {
                if (item.GameLabel !== Game.O) return;
                expect(item.GameLabel).to.deep.equal(Game.O);
            });
        });
    });
    describe("Set winner score", function() {
        playersContainer.SetUsersListAndAssignGameLabel(TestPlayersUserVsComputer);
        it("Increment winner score", function() {
            playersContainer.IncrementActiveUserScore();
            expect(playersContainer.GetActiveUser().Score).to.deep.equal(1);
        });
    });
});