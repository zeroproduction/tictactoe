import Game from "@Model/Game";

const expect = require("chai").expect;
import Constants from "@Root/constants";
import GameState from "@Engine/GameState";
import RenderLayout from "@Engine/RenderLayout";
import TestUserHelper from "./helpers/TestUserHelpers";

describe("RenderLayoutTests", function() {

    const renderLayout = new RenderLayout();
    const gameState = new GameState(
        TestUserHelper, Constants.USER_CONSTANTS.COMPUTER);

    describe("Build layout", function() {
        renderLayout.Buildlayout();
        it("Has 9 squares", function() {
            let squares = renderLayout.GetAllSquares();
            expect(squares.length).to.deep.equal(gameState.SquaresCount);
        });
    });
    describe("Mark one square", function() {
        let setSquareValueById = renderLayout.SetSquareValueById(2, Game.O);
        it("Has 1 marked squares", function() {
            expect(setSquareValueById).to.not.equal(false);
        });
        it("Has 8 unmarked squares", function() {
            let unselectedSquaresLength =  renderLayout.GetUnselectedSquares().length;
            expect(unselectedSquaresLength).to.deep.equal(8);
        });
    });
});