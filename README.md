# tictactoe

> A Node, SocketIO & Vue.js project

## Preparation

``` bash
# install all dependencies (installs Node and VueJS dependencies)
npm install-all-deps
```

## Server Build Setup

``` bash
# compile server files and start server
npm run compile-start-server

OR

# compile server files
npm run compile-server
npm run start-server
```

## VueJS Build Setup

``` bash
# compile server files
npm run start-vue
```


## Run server tests
``` bash
npm run tests
```